import React from 'react'
import CardItem from '../CardItem';
import './styles.css';

function Cards() {
      return (
            <div className="cards">
                  <h1>Check out theese EPIC Destinations!</h1>
                  <div className="cards__container">
                        <div className="cards__wrapper">
                              <ul className="cards__items">
                                    <CardItem 
                                    src="images/img-9.jpg"
                                    text="Explore the hidden waterfall deep inside the Amazon Jungle"
                                    label="Adventure"
                                    path="/services"
                                    />
                                     <CardItem 
                                    src="images/img-2.jpg"
                                    text="Travel through the Islands of Bali in a priate Cruise"
                                    label="Luxury"
                                    path="/services"
                                    />
                                     <CardItem 
                                    src="images/img-4.jpg"
                                    text="Discover unbelivable places in the Indian Ocean on the private plain"
                                    label="Adventure"
                                    path="/services"
                                    />
                                      <CardItem 
                                    src="images/img-5.jpg"
                                    text="Visit Monaco and drive through its Luxury Places!"
                                    label="Luxury"
                                    path="/services"
                                    />
                                      <CardItem 
                                    src="images/img-8.jpg"
                                    text="Get lost in the desert, feel the freedom!"
                                    label="Adventure"
                                    path="/services"
                                    />
                                   
                              </ul>
                        </div>
                  </div>

                  
            </div>
      )
}

export default Cards
